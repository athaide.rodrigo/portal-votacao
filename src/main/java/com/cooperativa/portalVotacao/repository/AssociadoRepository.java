package com.cooperativa.portalVotacao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cooperativa.portalVotacao.model.Associado;

@Repository
public interface AssociadoRepository extends JpaRepository<Associado, Long> {
	
}
