package com.cooperativa.portalVotacao.service;

import java.util.List;

import com.cooperativa.portalVotacao.model.Pauta;

public interface PautaService {
	
	List<Pauta> listarTodos();
	Pauta listarPorId(Long id);
	Pauta cadastrar(Pauta pauta) throws Exception;
	Pauta atualizar(Pauta pauta);
	void remover(Long id);

}
