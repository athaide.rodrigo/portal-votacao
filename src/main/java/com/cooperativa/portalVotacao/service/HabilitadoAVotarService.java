package com.cooperativa.portalVotacao.service;

import com.cooperativa.portalVotacao.model.Associado;
import com.cooperativa.portalVotacao.model.HabilitadoAVotar;

public interface HabilitadoAVotarService {

	HabilitadoAVotar associadoHabilitadoAVotar(Associado associado); 
}
