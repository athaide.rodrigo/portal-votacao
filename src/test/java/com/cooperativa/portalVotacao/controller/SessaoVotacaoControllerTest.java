package com.cooperativa.portalVotacao.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.util.Date;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import com.cooperativa.portalVotacao.controller.principal.HomeController;
import com.cooperativa.portalVotacao.controller.secundario.SessaoVotacaoController;
import com.cooperativa.portalVotacao.model.Pauta;
import com.cooperativa.portalVotacao.model.SessaoVotacao;
import com.cooperativa.portalVotacao.service.AssociadoService;
import com.cooperativa.portalVotacao.service.HabilitadoAVotarService;
import com.cooperativa.portalVotacao.service.PautaService;
import com.cooperativa.portalVotacao.service.SessaoVotacaoService;
import com.cooperativa.portalVotacao.service.VotoService;

import io.restassured.http.ContentType;

@WebMvcTest
public class SessaoVotacaoControllerTest {

	@Autowired
	private SessaoVotacaoController sessaoVotacaoController;
	
	@SuppressWarnings("unused")
	@Autowired
	private HomeController homeController;

	@MockBean
	private AssociadoService associadoService;
	
	@MockBean
	private HabilitadoAVotarService habilitadoAVotarService;
	@MockBean
	private PautaService pautaService;
	@MockBean
	private SessaoVotacaoService sessaoVotacaoService;
	@MockBean
	private VotoService votoService;

	@BeforeEach
	public void setup() {
		standaloneSetup(this.sessaoVotacaoController);
	}

	@Test
	void deveRetornarSucesso_QuandoPesquisarAssociado() throws Exception {

		Pauta pauta = new Pauta();
		pauta.setId(1L);
		pauta.setTitulo("teste titulo");
		pauta.setDescricao("desc teste");
		
		SessaoVotacao sessaoVotacao = new SessaoVotacao();
		sessaoVotacao.setDataHoraAberturaVotacao(new Date());
		sessaoVotacao.setTempoVotacao(20);
		sessaoVotacao.setId(1L);
		sessaoVotacao.setPauta(pauta);
		
		when(this.sessaoVotacaoService.listarPorId(1L)).thenReturn(sessaoVotacao);

		given()
			.accept(ContentType.JSON)
		.when()
			.get("/sessaoVotacao/{id}", "1")
		.then()
			.statusCode(HttpStatus.OK.value());
		assertThat(sessaoVotacao.getTempoVotacao()).isEqualTo(20);
	}

	@Test
	void deveRetornarNaoEncontrado_QuandoPesquisarAssociado() throws Exception {
		
		SessaoVotacao sessaoVotacao = new SessaoVotacao();
		
		when(this.sessaoVotacaoService.listarPorId(5L)).thenReturn(sessaoVotacao);
		
		given()
			.accept(ContentType.JSON)
		.when()
			.get("/sessaoVotacao/{id}", "5")
		.then()
			.statusCode(HttpStatus.OK.value());
		
		assertThat(sessaoVotacao.getTempoVotacao()).isEqualTo(0);
	}

}
