package com.cooperativa.portalVotacao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cooperativa.portalVotacao.model.Pauta;
import com.cooperativa.portalVotacao.repository.PautaRepository;
import com.cooperativa.portalVotacao.service.PautaService;

@Service 
public class PautaServiceImpl implements PautaService{
	
	@Autowired
	private PautaRepository repository;

	@Override
	public List<Pauta> listarTodos() {
		return this.repository.findAll();
	}

	@Override
	public Pauta listarPorId(Long id) {
		return this.repository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Pauta não existe"));
	}

	@Override
	public Pauta cadastrar(Pauta pauta) throws Exception {
		return this.repository.save(pauta);
	}

	@Override
	public Pauta atualizar(Pauta pauta) {
		return this.repository.save(pauta);
	}

	@Override
	public void remover(Long id) {
		this.repository.deleteById(id);
	}

}
