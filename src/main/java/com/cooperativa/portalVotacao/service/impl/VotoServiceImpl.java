package com.cooperativa.portalVotacao.service.impl;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cooperativa.portalVotacao.model.Associado;
import com.cooperativa.portalVotacao.model.SessaoVotacao;
import com.cooperativa.portalVotacao.model.SimNao;
import com.cooperativa.portalVotacao.model.Voto;
import com.cooperativa.portalVotacao.model.VotoDTO;
import com.cooperativa.portalVotacao.repository.VotoRepository;
import com.cooperativa.portalVotacao.service.AssociadoService;
import com.cooperativa.portalVotacao.service.HabilitadoAVotarService;
import com.cooperativa.portalVotacao.service.SessaoVotacaoService;
import com.cooperativa.portalVotacao.service.VotoService;

@Service
public class VotoServiceImpl implements VotoService {

	@Autowired
	private VotoRepository repository;
	@Autowired
	private AssociadoService associadoService;
	@Autowired
	private SessaoVotacaoService sessaoVotacaoService;
	@Autowired
	private HabilitadoAVotarService habilitadoAVotarService;

	@Override
	public List<Voto> listarTodos() {
		return this.repository.findAll();
	}

	@Override
	public Voto listarPorId(Long id) {
		return this.repository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Voto não existe"));
	}

	@Override
	public Voto cadastrar(Voto voto) throws Exception {
		return this.repository.save(voto);
	}

	@Override
	public Voto atualizar(Voto voto) {
		return this.repository.save(voto);
	}

	@Override
	public void remover(Long id) {
		this.repository.deleteById(id);
	}

	@Override
	public Voto votar(VotoDTO voto) {
		SessaoVotacao sessaoVotacao = this.sessaoVotacaoService.listarPorId(voto.getIdSessaoVotacao());
		Associado associado = this.associadoService.listarPorId(voto.getIdAssociado());

		Voto votoCadastrar = null;
		
		for (Voto votoCadastrado : sessaoVotacao.getListaDeVotos()) {
			if(votoCadastrado.getAssociado().getCpf() == associado.getCpf()) {
				votoCadastrar = votoCadastrado;
			}
		}

		if (votoCadastrar == null) {
			
			votoCadastrar = new Voto();
			votoCadastrar.setAssociado(this.associadoService.listarPorId(voto.getIdAssociado()));
			if (voto.getValor().equalsIgnoreCase("SIM")) {
				votoCadastrar.setValor(SimNao.SIM);
			} else {
				votoCadastrar.setValor(SimNao.NAO);
			}

			Date horarioAbertura = sessaoVotacao.getDataHoraAberturaVotacao();

			if (horarioAbertura != null) {
				int tempoVotacao = sessaoVotacao.getTempoVotacao() == 0 ? 1
						: sessaoVotacao.getTempoVotacao();

				Calendar cal = Calendar.getInstance();
				cal.setTime(horarioAbertura);
				cal.add(Calendar.MINUTE, tempoVotacao);

				if (new Date().before(cal.getTime())
						&& (habilitadoAVotarService.associadoHabilitadoAVotar(votoCadastrar.getAssociado()).getStatus()
								.equalsIgnoreCase("ABLE_TO_VOTE"))) {
					sessaoVotacao.getListaDeVotos().add(this.repository.save(votoCadastrar));
					this.sessaoVotacaoService.atualizar(sessaoVotacao);
				}
			}
		}

		return null;
	}

}
