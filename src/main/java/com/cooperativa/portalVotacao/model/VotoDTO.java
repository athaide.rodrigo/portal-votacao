package com.cooperativa.portalVotacao.model;

import lombok.Data;

@Data
public class VotoDTO {

	private Long IdAssociado;
	private String valor;
	private Long idSessaoVotacao;
}
