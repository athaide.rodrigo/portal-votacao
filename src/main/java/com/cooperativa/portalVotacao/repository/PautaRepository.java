package com.cooperativa.portalVotacao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cooperativa.portalVotacao.model.Pauta;

@Repository
public interface PautaRepository extends JpaRepository<Pauta, Long> {
	
}
