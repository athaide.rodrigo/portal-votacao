package com.cooperativa.portalVotacao.controller.secundario;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cooperativa.portalVotacao.model.SessaoVotacao;
import com.cooperativa.portalVotacao.model.SessaoVotacaoDTO;
import com.cooperativa.portalVotacao.service.SessaoVotacaoService;

@RestController
@RequestMapping("/sessaoVotacao")
public class SessaoVotacaoController {
	
	@Autowired
	private SessaoVotacaoService sessaoVotacaoService;
	
	Logger logger = Logger.getLogger(SessaoVotacaoController.class);
	
	@GetMapping
	public ResponseEntity<List<SessaoVotacao>> listarTodos() {
		logger.info("Hello world");
		return new ResponseEntity<List<SessaoVotacao>>(this.sessaoVotacaoService.listarTodos(),
				HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<SessaoVotacao> listarPorId(@PathVariable String id) {
		return new ResponseEntity<SessaoVotacao>(this.sessaoVotacaoService.listarPorId(new Long(id)),
				HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<SessaoVotacao> cadastrar(@RequestBody SessaoVotacaoDTO sessaoVotacao, BindingResult result) throws Exception {
		if(result.hasErrors()) {
			SessaoVotacao sessaoVotacaoErro = new SessaoVotacao();
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<SessaoVotacao>(sessaoVotacaoErro, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<SessaoVotacao>(this.sessaoVotacaoService.cadastrar(sessaoVotacao),
				HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<SessaoVotacao> atualizar(@PathVariable String id, @Valid @RequestBody SessaoVotacao sessaoVotacao, BindingResult result) {
		sessaoVotacao.setId(new Long(id));
		if(result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<SessaoVotacao>(sessaoVotacao, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<SessaoVotacao>(this.sessaoVotacaoService.atualizar(sessaoVotacao),
				HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable String id, BindingResult result) {
		this.sessaoVotacaoService.remover(new Long(id));
		if(result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<String>("Erro ao remover Sessão de Votação.", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("Sessão de Votação removida com sucesso.",
				HttpStatus.OK);
	}

}
