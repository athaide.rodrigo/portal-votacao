package com.cooperativa.portalVotacao.model;

import lombok.Data;

@Data
public class SessaoVotacaoDTO {

	private Long idPauta;
	private int tempoVotacao;
	
}
