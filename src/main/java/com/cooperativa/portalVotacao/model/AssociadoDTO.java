package com.cooperativa.portalVotacao.model;

import lombok.Data;

@Data
public class AssociadoDTO {

	private String cpf;
}
