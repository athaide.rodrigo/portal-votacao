package com.cooperativa.portalVotacao.controller.principal;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cooperativa.portalVotacao.model.AbrirSessaoVotacaoDTO;
import com.cooperativa.portalVotacao.model.Associado;
import com.cooperativa.portalVotacao.model.AssociadoDTO;
import com.cooperativa.portalVotacao.model.Pauta;
import com.cooperativa.portalVotacao.model.PautaDTO;
import com.cooperativa.portalVotacao.model.SessaoVotacao;
import com.cooperativa.portalVotacao.model.SessaoVotacaoDTO;
import com.cooperativa.portalVotacao.model.Voto;
import com.cooperativa.portalVotacao.model.VotoDTO;
import com.cooperativa.portalVotacao.service.AssociadoService;
import com.cooperativa.portalVotacao.service.PautaService;
import com.cooperativa.portalVotacao.service.SessaoVotacaoService;
import com.cooperativa.portalVotacao.service.VotoService;

import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping("/cooperativa")
public class HomeController {

	Logger logger = Logger.getLogger(HomeController.class);
	ModelMapper mapper = new ModelMapper();

	@Autowired
	private PautaService pautaService;
	@Autowired
	private SessaoVotacaoService sessaoVotacaoService;
	@Autowired
	private VotoService votoService;
	@Autowired
	private AssociadoService associadoService;

	@ApiOperation(value = "Cadastrar um Associado")
	@PostMapping("/cadastrar-associado")
	public ResponseEntity<Associado> cadastrarAssociado(@RequestBody AssociadoDTO associado, BindingResult result)
			throws Exception {
		
		Associado associadoCadastrar = this.mapper.map(associado, Associado.class);
		
		if (result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			logger.error("erro ao cadastrar associado");
			return new ResponseEntity<Associado>(associadoCadastrar, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Associado>(this.associadoService.cadastrar(associadoCadastrar), HttpStatus.OK);
	}

	@ApiOperation(value = "Cadastrar uma Pauta")
	@PostMapping("/cadastrar-pauta")
	public ResponseEntity<Pauta> cadastrarPauta(@RequestBody PautaDTO pauta, BindingResult result)
			throws Exception {
		
		Pauta pautaCadastrar = this.mapper.map(pauta, Pauta.class);
		
		if (result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			logger.error("erro ao cadastrar pauta");
			return new ResponseEntity<Pauta>(pautaCadastrar, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Pauta>(this.pautaService.cadastrar(pautaCadastrar), HttpStatus.OK);
	}

	@ApiOperation(value = "Cadastrar uma Sessão de votação")
	@PostMapping("/cadastrar-sessao-votacao")
	public ResponseEntity<SessaoVotacao> cadastrarSessaoVotacao(@RequestBody SessaoVotacaoDTO sessaoVotacao, BindingResult result)
			throws Exception {
		if (result.hasErrors()) {
			SessaoVotacao sessaoVotacaoErro = new SessaoVotacao();
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			logger.error("erro ao cadastrar Sessão de votação");
			return new ResponseEntity<SessaoVotacao>(sessaoVotacaoErro, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<SessaoVotacao>(this.sessaoVotacaoService.cadastrar(sessaoVotacao), HttpStatus.OK);
	}

	@ApiOperation(value = "Abrir uma Sessão de Votação")
	@PostMapping("/abrir-sessao-votacao")
	public ResponseEntity<SessaoVotacao> abrirSessaoVotacao(@RequestBody AbrirSessaoVotacaoDTO abrirSessaoVotacaoDTO,
			BindingResult result) throws Exception {
		SessaoVotacao sessaoVotacao = this.sessaoVotacaoService.listarPorId(abrirSessaoVotacaoDTO.getIdSessaoVotacao());
		if (result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			logger.error("erro ao abrir Sessão de votação");
			return new ResponseEntity<SessaoVotacao>(sessaoVotacao, HttpStatus.BAD_REQUEST);
		}
		sessaoVotacao.setDataHoraAberturaVotacao(new Date());
		return new ResponseEntity<SessaoVotacao>(this.sessaoVotacaoService.atualizar(sessaoVotacao), HttpStatus.OK);
	}

	@ApiOperation(value = "Votar em uma Sessão aberta")
	@PostMapping("/votar")
	public ResponseEntity<Voto> votar(@RequestBody VotoDTO voto, BindingResult result) throws Exception {
		if (result.hasErrors()) {
			Voto votoErro = new Voto();
			votoErro.setAssociado(this.associadoService.listarPorId(voto.getIdAssociado()));
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			logger.error("erro ao votar");
			return new ResponseEntity<Voto>(votoErro, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Voto>(this.votoService.votar(voto), HttpStatus.OK);
	}

	@ApiOperation(value = "Contabilizar os votos de uma Sessão")
	@GetMapping("/contabilizar-resultado/{idSessaoVotacao}")
	public Pauta contabilizarResultadoPauta(@PathVariable String idSessaoVotacao) {
		SessaoVotacao sessaoVotacao = this.sessaoVotacaoService.contabilizarResultardo(new Long(idSessaoVotacao));
		return sessaoVotacao.getPauta();
	}
}
