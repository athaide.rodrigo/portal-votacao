package com.cooperativa.portalVotacao.controller.secundario;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cooperativa.portalVotacao.model.Associado;
import com.cooperativa.portalVotacao.model.AssociadoDTO;
import com.cooperativa.portalVotacao.service.AssociadoService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/associado")
public class AssociadoController {
	
	@Autowired
	private AssociadoService associadoService;
	
	Logger logger = Logger.getLogger(AssociadoController.class);
	ModelMapper mapper = new ModelMapper();
	
	@ApiOperation(value = "Retorna uma lista de Associados")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Retorna a lista de Associados"),
	    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
	    @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@GetMapping
	public ResponseEntity<List<Associado>> listarTodos() {
		logger.info("Hello world");
		return new ResponseEntity<List<Associado>>(this.associadoService.listarTodos(),
				HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Associado> listarPorId(@PathVariable String id) {
		return new ResponseEntity<Associado>(this.associadoService.listarPorId(new Long(id)),
				HttpStatus.OK);
	}
	
	@PostMapping("/cadastrar-associado")
	public ResponseEntity<Associado> cadastrar(@RequestBody AssociadoDTO associado, BindingResult result)
			throws Exception {
		
		Associado associadoCadastrar = this.mapper.map(associado, Associado.class);
		
		if (result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<Associado>(associadoCadastrar, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Associado>(this.associadoService.cadastrar(associadoCadastrar), HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Associado> atualizar(@PathVariable String id, @Valid @RequestBody Associado associado, BindingResult result) {
		if(result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<Associado>(associado, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Associado>(this.associadoService.atualizar(associado),
				HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable String id, BindingResult result) {
		if(result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<String>("Erro ao remover associado.", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("Associado removido com sucesso.",
				HttpStatus.OK);
	}

}
