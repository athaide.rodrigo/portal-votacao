package com.cooperativa.portalVotacao.model;

import lombok.Data;

@Data
public class AbrirSessaoVotacaoDTO {

	private Long idSessaoVotacao;
	
}
