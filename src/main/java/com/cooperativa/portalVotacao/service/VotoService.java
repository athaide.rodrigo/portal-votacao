package com.cooperativa.portalVotacao.service;

import java.util.List;

import com.cooperativa.portalVotacao.model.Voto;
import com.cooperativa.portalVotacao.model.VotoDTO;

public interface VotoService {
	
	List<Voto> listarTodos();
	Voto listarPorId(Long id);
	Voto cadastrar(Voto voto) throws Exception;
	Voto atualizar(Voto voto);
	void remover(Long id);
	Voto votar(VotoDTO voto);

}
