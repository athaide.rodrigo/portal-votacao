package com.cooperativa.portalVotacao.service;

import java.util.List;

import com.cooperativa.portalVotacao.model.SessaoVotacao;
import com.cooperativa.portalVotacao.model.SessaoVotacaoDTO;

public interface SessaoVotacaoService {
	
	List<SessaoVotacao> listarTodos();
	SessaoVotacao listarPorId(Long id);
	SessaoVotacao cadastrar(SessaoVotacaoDTO sessaoVotacao) throws Exception;
	SessaoVotacao atualizar(SessaoVotacao sessaoVotacao);
	void remover(Long id);
	SessaoVotacao contabilizarResultardo(Long idSessaoVotacao);

}
