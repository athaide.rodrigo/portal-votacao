package com.cooperativa.portalVotacao.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import com.cooperativa.portalVotacao.controller.principal.HomeController;
import com.cooperativa.portalVotacao.controller.secundario.PautaController;
import com.cooperativa.portalVotacao.model.Pauta;
import com.cooperativa.portalVotacao.service.AssociadoService;
import com.cooperativa.portalVotacao.service.HabilitadoAVotarService;
import com.cooperativa.portalVotacao.service.PautaService;
import com.cooperativa.portalVotacao.service.SessaoVotacaoService;
import com.cooperativa.portalVotacao.service.VotoService;

import io.restassured.http.ContentType;

@WebMvcTest
public class PautaControllerTest {

	@Autowired
	private PautaController pautaController;
	
	@SuppressWarnings("unused")
	@Autowired
	private HomeController homeController;

	@MockBean
	private AssociadoService associadoService;
	@MockBean
	private HabilitadoAVotarService habilitadoAVotarService;
	@MockBean
	private PautaService pautaService;
	@MockBean
	private SessaoVotacaoService sessaoVotacaoService;
	@MockBean
	private VotoService votoService;

	@BeforeEach
	public void setup() {
		standaloneSetup(this.pautaController);
	}

	@Test
	void deveRetornarSucesso_QuandoPesquisarPauta() throws Exception {

		Pauta pauta = new Pauta();
		pauta.setId(1L);
		pauta.setTitulo("teste titulo");
		pauta.setDescricao("desc teste");
		
		when(this.pautaService.listarPorId(1L)).thenReturn(pauta);

		given()
			.accept(ContentType.JSON)
		.when()
			.get("/pauta/{id}", "1")
		.then()
			.statusCode(HttpStatus.OK.value());
		assertThat(pauta.getTitulo()).isEqualTo("teste titulo");
	}

	@Test
	void deveRetornarNaoEncontrado_QuandoPesquisarAssociado() throws Exception {
		
		Pauta pauta = new Pauta();
		
		when(this.pautaService.listarPorId(5L)).thenReturn(pauta);
		
		given()
			.accept(ContentType.JSON)
		.when()
			.get("/pauta/{id}", "5")
		.then()
			.statusCode(HttpStatus.OK.value());
		
		assertThat(pauta.getTitulo()).isEqualTo(null);
	}

}
