package com.cooperativa.portalVotacao.service;

import java.util.List;

import com.cooperativa.portalVotacao.model.Associado;

public interface AssociadoService {
	
	List<Associado> listarTodos();
	Associado listarPorId(Long id);
	Associado cadastrar(Associado associado) throws Exception;
	Associado atualizar(Associado associado);
	void remover(Long id);

}
