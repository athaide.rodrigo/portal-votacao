package com.cooperativa.portalVotacao;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PortalVotacaoApplication {

	public static void main(String[] args) {
		SpringApplication.run(PortalVotacaoApplication.class, args);
		
	}
}
