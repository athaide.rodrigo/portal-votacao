package com.cooperativa.portalVotacao.model;

import lombok.Data;

@Data
public class PautaDTO {

	private String titulo;
	private String descricao;
	
}
