FROM adoptopenjdk/openjdk11:alpine-jre

WORKDIR /app

COPY target/votacao-0.0.1-SNAPSHOT.jar /app/votacao.jar

ENTRYPOINT ["java", "-jar", "votacao.jar"] 