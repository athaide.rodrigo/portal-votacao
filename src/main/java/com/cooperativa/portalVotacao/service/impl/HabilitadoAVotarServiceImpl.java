package com.cooperativa.portalVotacao.service.impl;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.client.WebClient;

import com.cooperativa.portalVotacao.model.Associado;
import com.cooperativa.portalVotacao.model.HabilitadoAVotar;
import com.cooperativa.portalVotacao.service.HabilitadoAVotarService;
import com.google.gson.Gson;

import reactor.core.publisher.Mono;

@Service
public class HabilitadoAVotarServiceImpl implements HabilitadoAVotarService{
	
	private Gson gson = new Gson();

	@Override
	public HabilitadoAVotar associadoHabilitadoAVotar(Associado associado) {
		
		Mono<String> monoHabilitadoAVotar = WebClient.builder()
				.baseUrl("https://user-info.herokuapp.com")
				.defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.TEXT_HTML_VALUE)
				.build().method(HttpMethod.GET).uri("/users/" + associado.getCpf())
				.accept(MediaType.TEXT_HTML).retrieve().bodyToMono(String.class);

		String retornoMono = monoHabilitadoAVotar.block();
		
		HabilitadoAVotar habilitadoAVotar = new HabilitadoAVotar();
		if(retornoMono != null) {
			habilitadoAVotar = this.gson.fromJson(retornoMono, HabilitadoAVotar.class);
		}

		return habilitadoAVotar;
	}

}
