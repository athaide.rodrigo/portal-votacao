package com.cooperativa.portalVotacao.model;

public enum SimNao {

	SIM(1, "Sim"),
	NAO(0, "Nao");
	
	private int valor;
	private String descricao;
	
	SimNao(int valor, String descricao) {
		this.valor = valor;
		this.descricao = descricao;
	}

	public int getValor() {
		return valor;
	}

	public String getDescricao() {
		return descricao;
	}
}
