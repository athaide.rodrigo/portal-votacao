package com.cooperativa.portalVotacao.model;

import lombok.Data;

@Data
public class HabilitadoAVotar {
	
	private String status; // ABLE_TO_VOTE OR UNABLE_TO_VOTE
	
}
