package com.cooperativa.portalVotacao.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import com.cooperativa.portalVotacao.controller.principal.HomeController;
import com.cooperativa.portalVotacao.controller.secundario.AssociadoController;
import com.cooperativa.portalVotacao.model.Associado;
import com.cooperativa.portalVotacao.service.AssociadoService;
import com.cooperativa.portalVotacao.service.HabilitadoAVotarService;
import com.cooperativa.portalVotacao.service.PautaService;
import com.cooperativa.portalVotacao.service.SessaoVotacaoService;
import com.cooperativa.portalVotacao.service.VotoService;

import io.restassured.http.ContentType;

@WebMvcTest
public class AssociadoControllerTest {

	@Autowired
	private AssociadoController associadoController;

	@SuppressWarnings("unused")
	@Autowired
	private HomeController homeController;

	@MockBean
	private AssociadoService associadoService;

	@MockBean
	private HabilitadoAVotarService habilitadoAVotarService;
	@MockBean
	private PautaService pautaService;
	@MockBean
	private SessaoVotacaoService sessaoVotacaoService;
	@MockBean
	private VotoService votoService;

	@BeforeEach
	public void setup() {
		standaloneSetup(this.associadoController);
	}

	@Test
	void deveRetornarSucesso_QuandoPesquisarAssociado() throws Exception {

		Associado associado = new Associado();
		associado.setId(1L);
		associado.setCpf("060.343.254-98");

		when(this.associadoService.listarPorId(1L)).thenReturn(associado);

		given()
			.accept(ContentType.JSON)
		.when()
			.get("/associado/{id}", "1")
		.then()
			.statusCode(HttpStatus.OK.value());
		assertThat(associado.getCpf()).isEqualTo("060.343.254-98");
	}

	@Test
	void deveRetornarNaoEncontrado_QuandoPesquisarAssociado() throws Exception {
		
		Associado associado = new Associado();
		
		when(this.associadoService.listarPorId(5L)).thenReturn(associado);
		
		given()
			.accept(ContentType.JSON)
		.when()
			.get("/associado/{id}", "5")
		.then()
			.statusCode(HttpStatus.OK.value());
		
		assertThat(associado.getCpf()).isEqualTo(null);
	}


}
