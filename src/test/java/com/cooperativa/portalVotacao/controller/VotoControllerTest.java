package com.cooperativa.portalVotacao.controller;

import static io.restassured.module.mockmvc.RestAssuredMockMvc.given;
import static io.restassured.module.mockmvc.RestAssuredMockMvc.standaloneSetup;
import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;

import com.cooperativa.portalVotacao.controller.principal.HomeController;
import com.cooperativa.portalVotacao.controller.secundario.VotoController;
import com.cooperativa.portalVotacao.model.Associado;
import com.cooperativa.portalVotacao.model.SimNao;
import com.cooperativa.portalVotacao.model.Voto;
import com.cooperativa.portalVotacao.service.AssociadoService;
import com.cooperativa.portalVotacao.service.HabilitadoAVotarService;
import com.cooperativa.portalVotacao.service.PautaService;
import com.cooperativa.portalVotacao.service.SessaoVotacaoService;
import com.cooperativa.portalVotacao.service.VotoService;

import io.restassured.http.ContentType;

@WebMvcTest
public class VotoControllerTest {

	@Autowired
	private VotoController votoController;
	
	@SuppressWarnings("unused")
	@Autowired
	private HomeController homeController;

	@MockBean
	private AssociadoService associadoService;
	
	@MockBean
	private HabilitadoAVotarService habilitadoAVotarService;
	@MockBean
	private PautaService pautaService;
	@MockBean
	private SessaoVotacaoService sessaoVotacaoService;
	@MockBean
	private VotoService votoService;

	@BeforeEach
	public void setup() {
		standaloneSetup(this.votoController);
	}

	@Test
	void deveRetornarSucesso_QuandoPesquisarVoto() throws Exception {

		Associado associado = new Associado();
		associado.setId(1L);
		associado.setCpf("060.343.254-98");
		
		Voto voto = new Voto();
		voto.setId(1L);
		voto.setValor(SimNao.SIM);
		voto.setAssociado(associado);
		
		when(this.votoService.listarPorId(1L)).thenReturn(voto);

		given()
			.accept(ContentType.JSON)
		.when()
			.get("/voto/{id}", "1")
		.then()
			.statusCode(HttpStatus.OK.value());
		assertThat(voto.getValor()).isEqualTo(SimNao.SIM);
	}

	@Test
	void deveRetornarNaoEncontrado_QuandoPesquisarAssociado() throws Exception {
		
		Voto voto = new Voto();
		
		when(this.votoService.listarPorId(1L)).thenReturn(voto);
		
		given()
			.accept(ContentType.JSON)
		.when()
			.get("/voto/{id}", "5")
		.then()
			.statusCode(HttpStatus.OK.value());
		
		assertThat(voto.getValor()).isEqualTo(null);
	}


}
