package com.cooperativa.portalVotacao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cooperativa.portalVotacao.model.Associado;
import com.cooperativa.portalVotacao.repository.AssociadoRepository;
import com.cooperativa.portalVotacao.service.AssociadoService;

@Service  
public class AssociadoServiceImpl implements AssociadoService{
	
	@Autowired
	private AssociadoRepository repository;

	@Override
	public List<Associado> listarTodos() {
		return this.repository.findAll();
	}

	@Override
	public Associado listarPorId(Long id) {
		return this.repository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Associado não existe"));
	}

	@Override
	public Associado cadastrar(Associado associado) throws Exception {
		return this.repository.save(associado);
	}

	@Override
	public Associado atualizar(Associado associado) {
		return this.repository.save(associado);
	}

	@Override
	public void remover(Long id) {
		this.repository.deleteById(id);
	}

}
