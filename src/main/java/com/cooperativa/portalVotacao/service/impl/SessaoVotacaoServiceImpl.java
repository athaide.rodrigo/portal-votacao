package com.cooperativa.portalVotacao.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.cooperativa.portalVotacao.model.SessaoVotacao;
import com.cooperativa.portalVotacao.model.SessaoVotacaoDTO;
import com.cooperativa.portalVotacao.model.SimNao;
import com.cooperativa.portalVotacao.model.Voto;
import com.cooperativa.portalVotacao.repository.SessaoVotacaoRepository;
import com.cooperativa.portalVotacao.service.PautaService;
import com.cooperativa.portalVotacao.service.SessaoVotacaoService;

@Service 
public class SessaoVotacaoServiceImpl implements SessaoVotacaoService{
	
	@Autowired
	private SessaoVotacaoRepository repository;
	@Autowired
	private PautaService pautaService;

	@Override
	public List<SessaoVotacao> listarTodos() {
		return this.repository.findAll();
	}

	@Override
	public SessaoVotacao listarPorId(Long id) {
		return this.repository.findById(id)
				.orElseThrow(() -> new IllegalArgumentException("Sessão de Votação não existe"));
	}

	@Override
	public SessaoVotacao cadastrar(SessaoVotacaoDTO sessaoVotacaoDTO) throws Exception {
		SessaoVotacao sessaoVotacao = new SessaoVotacao();
		
		sessaoVotacao.setPauta(this.pautaService.listarPorId(sessaoVotacaoDTO.getIdPauta()));
		sessaoVotacao.setTempoVotacao(sessaoVotacaoDTO.getTempoVotacao());
		
		return this.repository.save(sessaoVotacao);
	}

	@Override
	public SessaoVotacao atualizar(SessaoVotacao sessaoVotacao) {
		return this.repository.save(sessaoVotacao);
	}

	@Override
	public void remover(Long id) {
		this.repository.deleteById(id);
	}

	@Override
	public SessaoVotacao contabilizarResultardo(Long idSessaoVotacao) {
		
		SessaoVotacao sessaoVotacao = listarPorId(idSessaoVotacao);
		
		int votosSim = 0, votosNao = 0;
		
		for (Voto voto : sessaoVotacao.getListaDeVotos()) {
			if(voto.getValor() == SimNao.SIM) {
				votosSim++;
			} else {
				votosNao++;
			}
		}
		
		if(votosSim < votosNao) {
			sessaoVotacao.getPauta().setResultado("Rejeitada");
		} else if(votosSim > votosNao) {
			sessaoVotacao.getPauta().setResultado("Aprovada");
		} else {
			sessaoVotacao.getPauta().setResultado("Empate");
		}
		
		return this.atualizar(sessaoVotacao);
	}

}
