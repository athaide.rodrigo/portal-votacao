package com.cooperativa.portalVotacao.controller.secundario;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cooperativa.portalVotacao.model.Pauta;
import com.cooperativa.portalVotacao.service.PautaService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/pauta")
public class PautaController {
	
	@Autowired
	private PautaService pautaService;
	
	Logger logger = Logger.getLogger(PautaController.class);
	
	@ApiOperation(value = "Retorna uma lista de Pautas")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Retorna a lista de Associados"),
	    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
	    @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@GetMapping
	public ResponseEntity<List<Pauta>> listarTodos() {
		logger.info("Hello world");
		return new ResponseEntity<List<Pauta>>(this.pautaService.listarTodos(),
				HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Pauta> listarPorId(@PathVariable String id) {
		return new ResponseEntity<Pauta>(this.pautaService.listarPorId(new Long(id)),
				HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Pauta> cadastrar(@Valid @RequestBody Pauta pauta, BindingResult result) throws Exception {
		if(result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<Pauta>(pauta, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Pauta>(this.pautaService.cadastrar(pauta),
				HttpStatus.OK);
	}

	@PutMapping("/{id}")
	public ResponseEntity<Pauta> atualizar(@PathVariable String id, @Valid @RequestBody Pauta pauta, BindingResult result) {
		if(result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<Pauta>(pauta, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Pauta>(this.pautaService.atualizar(pauta),
				HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable String id, BindingResult result) {
		if(result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<String>("Erro ao remover Pauta.", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("Pauta removida com sucesso.",
				HttpStatus.OK);
	}

}
