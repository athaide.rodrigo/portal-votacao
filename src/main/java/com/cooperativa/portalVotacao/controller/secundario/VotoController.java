package com.cooperativa.portalVotacao.controller.secundario;

import java.util.ArrayList;
import java.util.List;

import javax.validation.Valid;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cooperativa.portalVotacao.model.Voto;
import com.cooperativa.portalVotacao.service.VotoService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/voto")
public class VotoController {
	
	@Autowired
	private VotoService votoService;
	
	Logger logger = Logger.getLogger(VotoController.class);
	
	@ApiOperation(value = "Retorna uma lista de Votos")
	@ApiResponses(value = {
	    @ApiResponse(code = 200, message = "Retorna a lista de Votos"),
	    @ApiResponse(code = 403, message = "Você não tem permissão para acessar este recurso"),
	    @ApiResponse(code = 500, message = "Foi gerada uma exceção"),
	})
	@GetMapping
	public ResponseEntity<List<Voto>> listarTodos() {
		logger.info("Hello world");
		return new ResponseEntity<List<Voto>>(this.votoService.listarTodos(),
				HttpStatus.OK);
	}

	@GetMapping("/{id}")
	public ResponseEntity<Voto> listarPorId(@PathVariable String id) {
		return new ResponseEntity<Voto>(this.votoService.listarPorId(new Long(id)),
				HttpStatus.OK);
	}
	
	@PostMapping
	public ResponseEntity<Voto> cadastrar(@RequestBody Voto voto, BindingResult result) throws Exception {
		Voto votoErro = new Voto();
		if(result.hasErrors() ) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<Voto>(votoErro, HttpStatus.BAD_REQUEST);
		}
		
		Voto votoRetorno = this.votoService.cadastrar(voto);
		
		if(votoRetorno != null) {
			return new ResponseEntity<Voto>(this.votoService.cadastrar(voto),
					HttpStatus.OK);
		} else {
			return new ResponseEntity<Voto>(votoErro, HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/{id}")
	public ResponseEntity<Voto> atualizar(@PathVariable String id, @Valid @RequestBody Voto voto, BindingResult result) {
		if(result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<Voto>(voto, HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<Voto>(this.votoService.atualizar(voto),
				HttpStatus.OK);
	}
	
	@DeleteMapping("/{id}")
	public ResponseEntity<String> delete(@PathVariable String id, BindingResult result) {
		if(result.hasErrors()) {
			List<String> erros = new ArrayList<String>();
			result.getAllErrors().forEach(erro -> erros.add(erro.getDefaultMessage()));
			return new ResponseEntity<String>("Erro ao remover Voto.", HttpStatus.BAD_REQUEST);
		}
		return new ResponseEntity<String>("Voto removido com sucesso.",
				HttpStatus.OK);
	}

}
