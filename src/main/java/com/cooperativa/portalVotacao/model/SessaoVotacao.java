package com.cooperativa.portalVotacao.model;

import java.util.Date;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import lombok.Data;

@Data
@Entity
@Table(name="SESSOES_VOTACAO")
public class SessaoVotacao {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	@OneToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "id_pauta_fk", referencedColumnName = "id")
	private Pauta pauta;
	private Date dataHoraAberturaVotacao;
	private int tempoVotacao;
	
	@OneToMany(cascade = CascadeType.PERSIST)
	@JoinColumn(name = "id_voto_fk", referencedColumnName="id")
	private List<Voto> listaDeVotos;
	
}
